---
title: گیت لب
employee: 1200
industry: software
type: private
business:
  - onlineService
  - openCore
marketCap: 1000
revenue: 100
links:
  website: https://www.gitlab.com/
  wikipedia: https://en.wikipedia.org/wiki/Gitlab
---
